import json

from django.http import HttpResponse
from django.conf.urls import patterns, url

from touchtechnology.common.sites import Application


class SimpleTestSite(Application):

    def __init__(self, name='test', app_name='test', **kwargs):
        super(SimpleTestSite, self).__init__(
            name=name, app_name=app_name, **kwargs)

    def get_urls(self):
        urlpatterns = patterns(
            '',
            url(r'^$', self.index, name='index'),
        )
        return urlpatterns

    def index(self, request, *args, **kwargs):
        res = HttpResponse(content_type='text/plain')
        json.dump(self.kwargs, res)
        return res
