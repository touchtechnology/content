#!/bin/bash

export PYTHONPATH=$PYTHONPATH:$(pwd)

pushd touchtechnology/content
django-admin makemessages -l en-AU -l de -l fr -l ja
django-admin compilemessages
popd
