from os.path import dirname, join as basejoin, realpath

join = lambda *args: realpath(basejoin(*args))

PROJECT_DIR = join(dirname(__file__), '..')

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
    }
}

DEFAULT_FILE_STORAGE = 'touchtechnology.common.storage.FileSystemStorage'

TIME_ZONE = 'Australia/Sydney'
LANGUAGE_CODE = 'en-AU'

USE_TZ = True

MEDIA_ROOT = join(PROJECT_DIR, 'media')
MEDIA_URL = '/media/'

STATIC_URL = '/static/'

SECRET_KEY = '2bksb4rhbv7i1$!5xzux0&amp;&amp;sl2@de@k6sxb4(zlj4l)+xds#2i'
SITE_ID = 1

ROOT_URLCONF = 'test_app.urls'
WSGI_APPLICATION = 'test_app.wsgi.application'

INSTALLED_APPS = (
    'mptt',
    'guardian',
    'bootstrap3',
    'django_gravatar',

    'touchtechnology.common',
    'touchtechnology.admin',
    'touchtechnology.content',
    'example_app',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
)

FIXTURE_DIRS = (
    join(PROJECT_DIR, 'fixtures'),
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

ANONYMOUS_USER_ID = -1
ANONYMOUS_DEFAULT_USERNAME_VALUE = 'anonymous'

TOUCHTECHNOLOGY_SITEMAP_ROOT = 'home'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
    'touchtechnology.common.context_processors.env',
    'touchtechnology.common.context_processors.query_string',
    'touchtechnology.common.context_processors.site',
    'touchtechnology.common.context_processors.tz',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'touchtechnology.common.middleware.TimezoneMiddleware',
    'touchtechnology.content.middleware.SitemapNodeMiddleware',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)-10s %(name)-30s %(asctime)-27s '
                      '%(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
    },
    'handlers': {
        'null': {
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'touchtechnology': {
            'handlers': ['null'],
            'level': 'ERROR',
        },
    },
}
