from django.conf.urls import patterns, include, url
from django.contrib.admin import autodiscover

from touchtechnology.admin import sites as admin
autodiscover()

from touchtechnology.common.sites import AccountsSite
accounts = AccountsSite()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'test_app.views.home', name='home'),
    # url(r'^test_app/', include('test_app.foo.urls')),
    url(r'^accounts/', include(accounts.urls)),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
