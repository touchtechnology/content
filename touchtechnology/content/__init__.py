import logging
from os.path import abspath, dirname, join

logger = logging.getLogger(__name__)

NAME = 'Content'
INSTALL = ()

with open(abspath(join(dirname(__file__), 'VERSION.txt'))) as ver:
    __version__ = ver.read().strip()

logger.debug('"%s"/"%s"' % (NAME, __version__))
