var tinyMCE{{ mode|title }}List = new Array(
{% for url, f in files %}
	["{{ f|slice:"2:"|escapejs }}", "{{ url|escapejs }}"]{% if not forloop.last %},{% endif %}
{% endfor %}
);
