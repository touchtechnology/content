from os.path import dirname, join

from django.conf import settings
from django.test.utils import override_settings
from test_plus.test import TestCase
from touchtechnology.common.models import SitemapNode

LISTJS_MEDIA_ROOT = join(settings.PROJECT_DIR, 'test_app/media/listjs')


class SimpleAdminTests(TestCase):

    fixtures = ['simple_admin_tests']

    def get_text(self, filename):
        return open(join(dirname(__file__), filename)).read()

    def test_sitemap_index(self):
        # superuser test
        with self.login(username='gary', password='password'):
            res = self.get('admin:content:index')
            text = """
                <a role="button" href="/admin/content/application/1/">
                    <i class="fa fa-pencil fa-fw"></i>
                    Edit
                </a>
            """
            self.assertContains(res, text=text, html=True)
        # staff user
        with self.login(username='john', password='password'):
            res = self.get('admin:content:index')
            text = "<span>Simple Test</span>"
            self.assertContains(res, text=text, html=True)

    def test_site_index_before(self):
        # res = self.get('simpletest:index')
        res = self.client.get('/')
        self.assertEqual(res.content, '{"key": "value"}')

    def test_sitemap(self):
        data = {
            'object_id': '1',
            'parent': '',
            'title': 'Revised Title',
            'short_title': '',
            'enabled': '1',
            'hidden_from_navigation': '0',
            'hidden_from_sitemap': '0',
            # 'slug': '',  # will not be on the form
            'kw-TOTAL_FORMS': '1',
            'kw-INITIAL_FORMS': '1',
            'kw-MAX_NUM_FORMS': '1000',
            'kw-0-id': '1',
            'kw-0-key': 'x',
            'kw-0-value': '"y"',
            'copy-TOTAL_FORMS': '1',
            'copy-INITIAL_FORMS': '0',
            'copy-MAX_NUM_FORMS': '1',
            'copy-0-node': '1',
            'copy-0-id': '',
        }

        with self.login(username='gary', password='password'):
            res = self.post(
                'admin:content:placeholder:edit', 1, data=data, follow=True)

            # make sure the POST was successful and we redirect to the index
            self.assertRedirects(res, self.reverse('admin:content:index'))

            # double check by seeing if the 'Revised Title' is in the response
            text = '<a href="%s">Revised Title</a>' % (
                self.reverse('admin:content:placeholder:edit', 1),)
            self.assertContains(res, text=text, html=True)

        # load the site and validate the output has changed
        # res = self.get('simpletest:index')
        res = self.client.get('/')
        self.assertEqual(res.content, '{"x": "y"}')

    @override_settings(MEDIA_ROOT=LISTJS_MEDIA_ROOT)
    def test_tinymce_link_listjs(self):
        with self.login(username='gary', password='password'):
            self.get_check_200('admin:content:media:listjs', mode='link')
            res = self.get('admin:content:media:listjs', mode='link')
            self.assertEquals(
                res.content, self.get_text('test_admin_link_list.js'))

    @override_settings(MEDIA_ROOT=LISTJS_MEDIA_ROOT)
    def test_tinymce_image_listjs(self):
        with self.login(username='gary', password='password'):
            self.get_check_200('admin:content:media:listjs', mode='image')
            res = self.get('admin:content:media:listjs', mode='image')
            self.assertEquals(
                res.content, self.get_text('test_admin_image_list.js'))

    def test_edit_page(self):
        with self.login(username='gary', password='password'):
            self.assertGoodView('admin:content:page:add')

            copy = """<p>Sample page data.</p>"""
            data = {
                'parent-form-parent': '',
                'parent-form-title': 'Sample',
                'parent-form-short_title': '',
                'parent-form-enabled': '1',
                'parent-form-hidden_from_navigation': '0',
                'parent-form-hidden_from_sitemap': '0',
                'parent-form-slug': '',
                'parent-form-slug_locked': '0',

                'form-template': '',
                'form-keywords': '',
                'form-description': '',

                'formset-TOTAL_FORMS': '1',
                'formset-INITIAL_FORMS': '0',
                'formset-MIN_NUM_FORMS': '0',
                'formset-MAX_NUM_FORMS': '1000',
                'formset-0-copy': copy,
                'formset-0-label': 'copy',
                'formset-0-sequence': '1',
                'formset-0-id': '',
                'formset-0-page': '',
            }
            self.post('admin:content:page:add', data=data)
            self.response_302()

            node = SitemapNode.objects.latest('pk')
            self.assertEqual(node.get_absolute_url(), '/sample/')

            # Unfortunately the SitemapNodeMiddleware does not appear to be
            # having an effect?
            # self.get(node.get_absolute_url())
            # self.response_200()
