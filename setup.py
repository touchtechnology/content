import glob
import os
import touchtechnology.content
from setuptools import setup, find_packages

version = touchtechnology.content.__version__
author = 'Touch Technology Pty Ltd'
author_email = 'support@touchtechnology.com.au'
url = 'https://bitbucket.org/touchtechnology/content/'

if os.environ.get('CI'):
    branch = os.environ.get('CI_BRANCH')
    if branch != 'master':
        suffix = ".dev" + os.environ.get('CI_BUILD_NUMBER')
        if not version.endswith(suffix):
            version += suffix
            with open(glob.glob('*/*/VERSION.txt')[0], 'w') as txt:
                txt.write(version)
        author = os.environ.get('CI_COMMITTER_NAME', author)
        author_email = os.environ.get('CI_COMMITTER_EMAIL', author_email)
        url = os.environ.get('CI_BUILD_URL', url)

setup(
    name='touchtechnology-content',
    version=version,
    author=author,
    author_email=author_email,
    url=url,
    description='Content Management System library.',
    packages=find_packages(exclude=["test_app", "example_app"]),
    install_requires=[
        'touchtechnology-admin>=3.2.9',
        'django-json-field>=0.5.7',
    ],
    include_package_data=True,
    namespace_packages=['touchtechnology'],
    zip_safe=False,
)
