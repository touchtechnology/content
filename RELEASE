RELEASES
--------

3.0.6 - Monday 14th March, 2016

 * Push applications urls to top of the dehydrated list of patterns

3.0.5 - Monday 25th January, 2016

 * Completed media manager

3.0.4 - Thursday 31st December, 2015

 * Fix for https://rollbar.com/TouchTechnology/FixjaNinja/items/485/
 * Add django-froala-editor for much improved WYSIWYG editing

3.0.3 - Saturday 12th December, 2015

 * Remove deprecated Django features - making it possible to use with Django 1.8

3.0.2 - Tuesday 1st December, 2015

 * HTTP caching improvements

3.0.1 - Thursday 26th February, 2015

 * django-guardian: track latest releases of common and admin libraries.

3.0 - Saturday 25th October, 2014

 *** Targets Django 1.7 ***

 * bug-4: Fix minor issue with deleting files in the root of the media directory.

2.3.2 - Friday 24th October, 2014

 * bug-4: Fixed deletion of files and folders in the media admin application.

2.3.1 - Sunday 22nd June, 2014

 * Revised SitemapNodeMiddleare to generate dynamic_urls without leaking memory

2.3 - Saturday 21st June, 2014

 * Make keyword arguments a JSONField to allow complex types to be represented

2.2.4 - Thursday 29th May, 2014

 * Remove python-dateutil from install_requires

2.2.3 - Monday 28th April, 2014

 * Fixed #2: added test cases for /admin/files/tinymce/link_list.js and /admin/files/tinymce/image_list.js [a97d057]

2.2.2 - Saturday 19th April, 2014

 * Fixed #1: Dynamic Application configuration changes take effect immediately [a2df3a8, 6a3f98f]

2.2.1 - Thursday 10th April, 2014

 * Fixed packaging to include templates and static media

2.2 - Wednesday 9th April, 2014

 * Major cleanup of dependencies, PEP8 and packaging refactor

2.1.1 - Thursday 22nd August, 2013

 * Fix templates that were missed in update for new {% url %} syntax [614b4cf]
 * Handle ImportError and ValueError for uninstalled applications [9611289]

2.1 - Saturday 17th August, 2013

 * Framework updates - compatability with Django 1.4-1.6 [9975e8a]
 * Multiple language support [f6c5ebb]
 * Fix run-away Application instance memory leak [214a8b3]
 * Switch to generic_edit and generic_delete methods of Application class [4ae747b]
 * Extend the template path expansion based on slug-hierarchy [e3cd1ab]
 * Add icons to indicate visibility in sitemap.xml & navigation structures [f4e65f6]
 * Remove {% block right_content %} from _page.html template [e9f231f]
 * Add .txt files to the list of file links for TinyMCE [e944f17]
